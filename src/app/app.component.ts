import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  userServiceSubcription:Subscription = Subscription.EMPTY;
  didActive = false;
  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userServiceSubcription = this.userService.booleanOfSubject.subscribe(
      (data)=>{
        this.didActive=data;
      }
    )
  }

  ngOnDestroy():void{
    this.userServiceSubcription.unsubscribe();
  }
}
